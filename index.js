let workMoney = 0;
let bankMoney = 0;
let loanMoney = 0;

let selectedLaptopPrice = 0;

let laptops = [];
let features = [];

const workMoneyDisplay = document.getElementById("workMoney");
const loanMoneyDisplay = document.getElementById("loanMoney");
const loanTextDisplay = document.getElementById("loanText");
const bankMoneyDisplay = document.getElementById("bankMoney");
const repayButtonDisplay = document.getElementById("repayButton");

const laptopsElement = document.getElementById("laptops");
const featuresElement = document.getElementById("features");
const featuresMainText = document.getElementById("featuresMainText");
const laptopImageElement = document.getElementById("laptopImage");
const laptopTitleElement = document.getElementById("laptopTitle");
const laptopDescriptionElement = document.getElementById("laptopDescription");
const laptopPriceElement = document.getElementById("laptopPrice");
const buyNowButtonElement = document.getElementById("buyNow");


/**
 * This function is made for the 'Work' button.
 * It adds money to the work balance.
 */
function work(){
    workMoney += 100;
    workMoneyDisplay.innerText = new Intl.NumberFormat('nl-NL', { style: 'currency', currency: 'EUR' }).format(workMoney);
}

/**
 * This function is made for the 'Bank' button.
 * It transfers work money to the bank balance.
 */
function bank(){
    //Checks if user has an open loan.
    if(loanMoney != 0){
        let workMoneyLoan = 0;
        //Gets 10% of the work money.
        workMoneyLoan = workMoney / 10;
        //Takes away 10% from the work money.
        workMoney -= workMoneyLoan;
        //Takes away the 10% of the work money from the outstanding loan.
        loanMoney -= workMoneyLoan;
        //The loan money will be added to the work money if the loan money is less than 0.
        if(loanMoney < 0){
            loanMoney = loanMoney * -1;
            workMoney += loanMoney;
            loanMoney = 0;
        }
        loanMoneyDisplay.innerText = new Intl.NumberFormat('nl-NL', { style: 'currency', currency: 'EUR' }).format(loanMoney);
        //If loan money is 0, hide the loan information on screen.
        if(loanMoney == 0){
            loanMoneyDisplay.hidden = true;
            loanTextDisplay.hidden = true;
            repayButtonDisplay.hidden = true;
        }
    }
    bankMoney += workMoney;
    workMoney = 0;
    workMoneyDisplay.innerText = new Intl.NumberFormat('nl-NL', { style: 'currency', currency: 'EUR' }).format(workMoney);
    bankMoneyDisplay.innerText = new Intl.NumberFormat('nl-NL', { style: 'currency', currency: 'EUR' }).format(bankMoney);
}

/**
 * This function is made for the 'Get a loan' button.
 * It checks if a loan already exists and if the loan has a valid input. (isNaN or higher than a euro cent)
 * The amount will be added to your bank balance if your input is valid.
 * The 'Loan' label will appear. This shows your outstanding loan.
 */
function getALoan(){
    if(loanMoney != 0){
        alert("You already have a loan.")
    }
    else{
        let isNumber = 0;

        isNumber = Number(prompt("How much do you want to loan?", ""));

        if(isNaN(isNumber) || isNumber < 0.01){
            alert("Your input was not a valid amount.")
        }
        else{
            loanMoney = isNumber;

            if(loanMoney > bankMoney * 2){
                alert("You cannot get a loan more than double your bank balance.")
                loanMoney = 0;
            }
            else{
                bankMoney += loanMoney;

                loanMoneyDisplay.innerText = new Intl.NumberFormat('nl-NL', { style: 'currency', currency: 'EUR' }).format(loanMoney);
                loanMoneyDisplay.hidden = false;
                loanTextDisplay.hidden = false;
                repayButtonDisplay.hidden = false;
                bankMoneyDisplay.innerText = new Intl.NumberFormat('nl-NL', { style: 'currency', currency: 'EUR' }).format(bankMoney);
            }
        }
    }
}
/**
 * This function is made for the 'Repay Loan' button.
 * This button is hidden. If a loan has been taken, the button will appear.
 * It repays the outstanding loan from your work balance.
 */
function repayLoan(){
    loanMoney -= workMoney;
    workMoney = 0;
    workMoneyDisplay.innerText = new Intl.NumberFormat('nl-NL', { style: 'currency', currency: 'EUR' }).format(workMoney);

    if(loanMoney < 0){
        loanMoney = loanMoney * -1;
        bankMoney += loanMoney;
        loanMoney = 0;
        bankMoneyDisplay.innerText = new Intl.NumberFormat('nl-NL', { style: 'currency', currency: 'EUR' }).format(bankMoney);
    }

    loanMoneyDisplay.innerText = new Intl.NumberFormat('nl-NL', { style: 'currency', currency: 'EUR' }).format(loanMoney);
    if(loanMoney == 0){
        loanMoneyDisplay.hidden = true;
        loanTextDisplay.hidden = true;
        repayButtonDisplay.hidden = true;
    }
}

/**
 * This function is made for the 'Buy Now' button.
 * A laptop will be bought after button press.
 * It checks if your bank balance is high enough to buy the selected laptop.
 * It uses a timeout, so the bank balance updates first.
 */
function buyNow(){
    if(bankMoney >= selectedLaptopPrice){
        bankMoney -= selectedLaptopPrice;
        bankMoneyDisplay.innerText = new Intl.NumberFormat('nl-NL', { style: 'currency', currency: 'EUR' }).format(bankMoney);

        setTimeout(function() {
            alert("Purchase has been completed. You are now the owner of the new laptop!");
        }, 100);
    }
    else{
        alert("You do not have enough money to purchase this laptop.")
    }
}
/**
 * Fetches the laptops from the api.
 */
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(result => laptops = result)
    .then(laptops => addLaptopsToMenu(laptops));

/**
 * Takes every laptop and adds it to the menu.
 */
const addLaptopsToMenu = (laptops) => {
    laptops.forEach(laptop => addLaptopToMenu(laptop));
}

/** 
 * Creates an option for every laptop in the database.
 * Adds the laptop id as value and title als laptop name.
 */
const addLaptopToMenu = (laptop) => {
    const laptopElement = document.createElement("option");
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    laptopsElement.appendChild(laptopElement);
}

/**
 * Changes the features that are displayed with each laptop.
 * Adds a new div per feature. It will remove the previous div's when a new feature is loaded.
 */
const handleFeatureAreaChange = e => {
    const selectedLaptop = laptops[e.target.selectedIndex - 1];
    featuresMainText.hidden = false;

    while(featuresElement.firstChild){
        featuresElement.removeChild(featuresElement.firstChild);
    }
    for (let i = 0; i < selectedLaptop.specs.length; i++) {
        const spec = document.createElement("div");
        spec.appendChild(document.createTextNode(selectedLaptop.specs[i]));
        featuresElement.appendChild(spec);
    }
}
/** 
 * Changes the info section that is displayed with each laptop.
 * This section is hidden on screen, until a new laptop is selected.
 */
const handleInfoSectionChange = e => {
    const selectedLaptop = laptops[e.target.selectedIndex - 1];

    laptopImageElement.hidden = false;
    laptopTitleElement.hidden = false;
    laptopDescriptionElement.hidden = false;
    laptopPriceElement.hidden = false;
    buyNowButtonElement.hidden = false;

    laptopImageElement.src = "https://noroff-komputer-store-api.herokuapp.com/assets/images/" + selectedLaptop.id + ".jpg";
    laptopImageElement.onerror = function(){
        laptopImageElement.src = "https://noroff-komputer-store-api.herokuapp.com/assets/images/" + selectedLaptop.id + ".png";
    }
    
    laptopTitleElement.innerText = selectedLaptop.title;
    laptopDescriptionElement.innerText = selectedLaptop.description;
    laptopPriceElement.innerText = new Intl.NumberFormat('nl-NL', { style: 'currency', currency: 'EUR' }).format(selectedLaptop.price);

    selectedLaptopPrice = selectedLaptop.price;
}

laptopsElement.addEventListener("change", handleFeatureAreaChange);
laptopsElement.addEventListener("change", handleInfoSectionChange);