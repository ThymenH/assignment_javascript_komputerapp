<!--
*** Thanks for checking out this README Template. If you have a suggestion that would
*** make this better, please fork the repo and create a pull request or simply open
*** an issue with the tag "enhancement".
*** Thanks again! Now go create something AMAZING! :D
***
***
***
*** To avoid retyping too much info. Do a search and replace for the following:
*** github_username, repo_name, twitter_handle, email
-->





<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->

<!-- PROJECT LOGO -->
<!-- <br />
<p align="center">
  <a href="https://github.com/github_username/repo_name">
    <img src="images/logo.png" alt="Logo" width="80" height="80">
  </a> -->

# Assignment - Javascript Komputer App


<!-- ABOUT THE PROJECT -->
## About The Project

This project was created as an assignment.
<br />
A dynamic webpage for earning money and buying laptops!

## Setup
To use the code you will need: 
- Visual Code
- Live Server VS add-on

<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.

### Installation

1. Clone the repo
```sh
git clone https://gitlab.com/ThymenH/assignment_javascript_komputerapp.git
```

<!-- USAGE EXAMPLES -->
## Usage

### The Bank
An area where you can store your funds and make bank loans.
1. The 'Get a loan' button will attempt to get a loan from the bank.
    * You cannot get a loan more than double your bank balance.
    * You cannot have more than one loan at once.
2. The 'Loan' displays your outstanding loan.

### Work
An area to increase your earnings, pay back loans and deposit cash into your bank balance.
1. The 'Bank' button transfers money from your work balance to your bank balance.
    * If you have an outstanding loan, 10% of your salary will first be deducted and transferred to the outstanding loan amount.
2. The 'Work' button increases your work balance at a rate of 100 each click.
3. The 'Repay Loan' button will be displayed once you have a loan. Upon clicking this button, the full value of your current pay goes towards
the outstanding loan. Any remaing funds after paying the loan will be transfered to your bank account.

### Laptops
An area to select and display information about the laptops and buy laptops.
1. The 'Select a laptop' box can be used to show the available laptops. Once a laptop is selected, features will be updated
and the laptop info section will be populated.
2. The 'Laptop Info Section' shows the image, name, and description as well as the price of the currently selected laptop.
3. The 'Buy Now' button will the selected laptop, if you have enough money in your bank.
